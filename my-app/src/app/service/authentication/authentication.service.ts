import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/app/environment';
import { SignInData } from 'src/app/model/SignInData';
import {OAuthService} from "angular-oauth2-oidc";
import { authConfig } from './authconfig';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  apiUrl:string = '';
  
  constructor(private router : Router, private http: HttpClient, private oauthService: OAuthService) { 
    this.configure();
  }
  
  private configure() {
    this.oauthService.configure(authConfig);
    this.oauthService.loadDiscoveryDocumentAndTryLogin(); // This method is trigger issuer uri
  }

  authenticate() {
    this.oauthService.initCodeFlow();
    window.sessionStorage.getItem("access_token");
  
    // this.generateTokenAndLogin(signIndata);
  }

  logout() {
    this.oauthService.revokeTokenAndLogout();
  }

  // generateTokenAndLogin(signIndata: SignInData) {
  //     let apiUrl = environment.apiUrl;
  //     this.http.post<any>(apiUrl+'/login/user', signIndata, {headers:{skip:"true"}, observe: 'response'}).subscribe(data =>  {
  //     window.localStorage.setItem("access_token", data.body.access_token);
  //     window.localStorage.setItem("refresh_token",data.body.refresh_token);
  //       console.log(data.body.access_token);
  //     this.router.navigate(['mainpage']);

  //   });
  // }

}
