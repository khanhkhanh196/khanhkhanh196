import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: "http://localhost:36931/realms/spring-boot-realm",
  // issuer: 'http://localhost:44839/realms/spring-boot-realm',
  redirectUri: window.location.origin,
  clientId: "spring-client",
  responseType: "code",
  strictDiscoveryDocumentValidation: true,
  scope: "openid profile email offline_access",
  requireHttps: false,
}