import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment';




@Injectable({
  providedIn: 'root'
})
export class ProductService{
  //  headers = new HttpHeaders({ Authorization: "Bearer " + window.localStorage.getItem("access_token") });

   baseURL = environment.apiUrl;
   URL = this.baseURL+'/rest/products';

  constructor(private httpClient: HttpClient) { 
  }

  readAll(): Observable<any> {
    return this.httpClient.get(this.URL);
  }

  read(id: string): Observable<any> {
    return this.httpClient.get(`${this.URL}/${id}`);
  }

  create(data:any): Observable<any> {
    return this.httpClient.post( this.URL, data);
  }

  update(id:any, data:any): Observable<any> {
    return this.httpClient.put(`${this.URL}/${id}`, data);
  }

  delete(id:any): Observable<any> {
    return this.httpClient.delete(`${this.URL}/${id}`);
  }
}
