import { Component } from '@angular/core';
import { ProductService } from '../../product.service';
import { Product } from 'src/app/model/Product';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.sass']
})
export class ProductListComponent {
  products: Array<Product> = [];
  currentProduct: Product | undefined;
  currentIndex = -1;
  name = '';

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.readProducts();
  }

  readProducts(): void {
    this.productService.readAll()
      .subscribe(
        products => {
          this.products = products;
          console.log(products);
        });
  }

  refresh(): void {
    this.readProducts();
    this.currentProduct = undefined;
    this.currentIndex = -1;
  }

  setCurrentProduct(product: Product, index:any): void {
    this.currentProduct = product;
    this.currentIndex = index;
  }
}
