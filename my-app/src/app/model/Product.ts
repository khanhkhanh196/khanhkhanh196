export class Product{
    public id:string;
    public name:string;
    public description:string;
    public available:boolean;

    constructor(id:string, name: string, description: string, available:boolean) {
      this.id = id;
       this.name = name;
       this.description = description;
       this.available = available;
    }

    getName() {
       return this.name;
    }

    getDescription() {
       return this.description;
    }

    getAvailable() {
        return this.available;
    }
}