import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, catchError, of, tap } from 'rxjs';
import { AuthenticationService } from '../service/authentication/authentication.service';
import { environment } from '../environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard  {

  constructor(private authenticationService: AuthenticationService, private router :Router,private http: HttpClient, private oauthService: OAuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let apiUrl = environment.apiUrl;
      let accessToken =  window.sessionStorage.getItem("access_token");
      const header = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      });

     return this.http.get<boolean>(apiUrl+'/general/check'
     , {headers : header}
     )
     .pipe(catchError( _ => {
      this.router.navigateByUrl('');
      return of(false);
    }));
 }
}
