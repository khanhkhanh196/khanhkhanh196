import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private http: HttpClient, private router: Router, private authenticationService : AuthenticationService) {}
  data:any;
  apiUrl :string = '';
  userName: string ='';
  password:string = ''; 
  formdata:any;
 
  
  ngOnInit() { 
    this.formdata = new FormGroup({ 
       userName: new FormControl(""),
       password: new FormControl("")
    });

 }

 onClickSubmit() {
  // this.userName = data.userName;
  // this.password = data.password;
  // const signinData = new SignInData(data.userName, data.password);

  this.authenticationService.authenticate();
}
}
