import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http'; 
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { MainpageComponent } from './main-page/mainpage/mainpage.component';
import { LoginComponent } from './login/login/login.component';
import { ProductCreateComponent } from './service/product/components/product-create/product-create.component';
import { ProductDetailsComponent } from './service/product/components/product-details/product-details.component';
import { ProductListComponent } from './service/product/components/product-list/product-list.component';
// import { HeaderInterceptor } from './interceptor/headerinterceptor';
import { OAuthLogger, OAuthModule, OAuthService, UrlHelperService } from 'angular-oauth2-oidc';

@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    LoginComponent,
    ProductCreateComponent,
    ProductDetailsComponent,
    ProductListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls : [
          'http://localhost:9090/general/check',
          'http://localhost:9090/rest/'
        ],
        sendAccessToken:true
      }
    }),
    FormsModule
  ],
  providers: [
    // OAuthService,
    // UrlHelperService,
    // { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
