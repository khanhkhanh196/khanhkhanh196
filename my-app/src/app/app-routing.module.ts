import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainpageComponent } from './main-page/mainpage/mainpage.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { ProductListComponent } from './service/product/components/product-list/product-list.component';
import { ProductDetailsComponent } from './service/product/components/product-details/product-details.component';
import { ProductCreateComponent } from './service/product/components/product-create/product-create.component';
const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: MainpageComponent},
  { path: 'mainpage/products', component: ProductListComponent
  , canActivate: [AuthGuard] 
},
  { path: 'mainpage/products/:id', component: ProductDetailsComponent
  , canActivate: [AuthGuard] 
},
  { path: 'mainpage/create', component: ProductCreateComponent
  , canActivate: [AuthGuard] 
}
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

