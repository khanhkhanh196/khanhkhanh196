{{- define "commons.labels" -}}
app: {{ .Values.containername }}
version: stable
{{- end}}


{{- define "commons.helmdata" -}}
app.kubernetes.io/name: {{ .Values.metadata }}
helm.sh/chart: {{ .Values.chart }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end}}

{{- define "commons.metadata" -}}
name: {{ .Values.metadata }}
labels:
  app: {{ .Values.metadata }}
{{- end}}

{{- define "commons.serviceMetadata" -}}
name: {{ .Values.serviceMetadata }}
labels:
  app: {{ .Values.serviceMetadata }}
{{- end}}

{{- define "commons.container" -}}
- image: {{ .Values.image }}
  name:  {{ .Values.containername }}
{{- end}}

