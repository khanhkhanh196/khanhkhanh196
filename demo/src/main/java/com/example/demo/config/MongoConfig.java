package com.example.demo.config;

import com.example.demo.object.UserRepository;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

import static java.util.Collections.singletonList;

@Configuration
@EnableMongoRepositories(basePackageClasses = UserRepository.class, mongoTemplateRef = "primaryMongoTemplate")
@EnableConfigurationProperties
@EnableMongoAuditing
//        (dateTimeProviderRef = "auditingDateTimeProvider")
public class MongoConfig {

//    @Bean(name = "auditingDateTimeProvider")
//    public DateTimeProvider dateTimeProvider() {
//        return () -> Optional.of(LocalDateTime.now());
//    }

//    @Bean
//    public MongoCustomConversions mongoCustomConversions() {
//        return new MongoCustomConversions(Arrays.asList(
//                new MongoOffsetDateTimeWriter(),
//                new MongoOffsetDateTimeReader()
//        ));
//    }

    @Bean(name = "primaryProperties")
    @ConfigurationProperties(prefix = "mongodb.primary")
    @Primary
    public MongoProperties primaryProperties() {
        return new MongoProperties();
    }

    @Bean(name = "primaryMongoClient")
    public MongoClient mongoClient(@Qualifier("primaryProperties") MongoProperties mongoProperties) {

        MongoCredential credential = MongoCredential
                .createCredential(mongoProperties.getUsername(), mongoProperties.getAuthenticationDatabase(), mongoProperties.getPassword());

        return MongoClients.create(MongoClientSettings.builder()
                .applyToClusterSettings(builder -> builder
                        .hosts(singletonList(new ServerAddress(mongoProperties.getHost(), mongoProperties.getPort()))))
                .credential(credential)
                .build());
    }

    @Primary
    @Bean(name = "primaryMongoDBFactory")
    public MongoDatabaseFactory mongoDatabaseFactory(
            @Qualifier("primaryMongoClient") MongoClient mongoClient,
            @Qualifier("primaryProperties") MongoProperties mongoProperties) {
        return new SimpleMongoClientDatabaseFactory(mongoClient, mongoProperties.getDatabase());
    }

    @Bean(name = "primaryMongoTemplate")
    public MongoTemplate mongoTemplate(@Qualifier("primaryMongoDBFactory") MongoDatabaseFactory mongoDatabaseFactory) {
//        MappingMongoConverter converter = new MappingMongoConverter(
//                new DefaultDbRefResolver(mongoDatabaseFactory), new MongoMappingContext());
//        converter.setCustomConversions(mongoCustomConversions());
//        converter.afterPropertiesSet();
        return new MongoTemplate(mongoDatabaseFactory);
    }


//    static class MongoOffsetDateTimeReader implements Converter<Document, OffsetDateTime> {
//
//        @Override
//        public OffsetDateTime convert(final Document document) {
//            final Date dateTime = document.getDate(MongoOffsetDateTimeWriter.DATE_FIELD);
//            final ZoneOffset offset = ZoneOffset.of(document.getString(MongoOffsetDateTimeWriter.OFFSET_FIELD));
//            return OffsetDateTime.ofInstant(dateTime.toInstant(), offset);
//        }
//
//    }
//
//    static class MongoOffsetDateTimeWriter implements Converter<OffsetDateTime, Document> {
//
//        public static final String DATE_FIELD = "dateTime";
//        public static final String OFFSET_FIELD = "offset";
//
//        @Override
//        public Document convert(final OffsetDateTime offsetDateTime) {
//            final Document document = new Document();
//            document.put(DATE_FIELD, Date.from(offsetDateTime.toInstant()));
//            document.put(OFFSET_FIELD, offsetDateTime.getOffset().toString());
//            return document;
//        }
//
//    }
}
