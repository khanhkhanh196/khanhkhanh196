package com.example.demo.config;

import com.example.demo.service.KeycloakRoleConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfig implements WebMvcConfigurer{
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new KeycloakRoleConverter());
        http
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/login/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/rest/**").hasAnyRole("user","administrator")
                .antMatchers("/rest/**").hasRole("administrator")
                .antMatchers("/general/**").hasAnyRole("administrator","user")
                .anyRequest()
                .authenticated()
                .and()
                .oauth2ResourceServer().jwt().jwtAuthenticationConverter(converter);
        return http.build();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
                .allowedHeaders("*")
                .exposedHeaders("WWW-Authenticate")
                .allowCredentials(false);
    }
}
