package com.example.demo.controller;

import com.example.demo.object.User;
import com.example.demo.object.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class UserController {
    @Autowired
    UserRepository repository;

    @GetMapping("/users")
    public List<User> getAllUser() {
        System.out.println("Success");
        return repository.findAll();
    }

    @GetMapping("/email/{email}")
    public User getAllUserEmail(@PathVariable("email") String email)  {
        return repository.findByEmail(email);
    }

    @GetMapping("/test")
    public String test() {
        return "Success";
    }

}
