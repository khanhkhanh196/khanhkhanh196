package com.example.demo.controller;

import com.example.demo.object.Product;
import com.example.demo.object.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class ProductController {
    @Autowired
    ProductRepository repository;
    @Autowired
    private RestTemplate secondRestTemplate;
    @Autowired
    LoginController loginController;
    @GetMapping("/products")
    public List<Product> getAllProduct() {
        return repository.findAll();
    }

    @PostMapping("/products")
    public Product createNew(@RequestBody Product product) {
        return repository.save(product);
    }

    @PutMapping("/products/{id}")
    public boolean updateProduct(@PathVariable String id, @RequestBody Product productUpdate) {
        Product product = repository.findById(id).orElse(null);
        if( product != null) {
            product.setAvailable(productUpdate.getAvailable());
            product.setName(productUpdate.getName());
            product.setDescription(productUpdate.getDescription());
            repository.save(product);
            return true;
        }
        return false;
    }

    @DeleteMapping("/products/{id}")
    public boolean delete(@PathVariable String id) {
        Product product = repository.findById(id).orElse(null);
        if( product != null) {
            repository.delete(product);
            return true;
        }
        return false;
    }

    @GetMapping("/products/{id}")
    public Product getOneProduct(@PathVariable String id) {
        Product product = repository.findById(id).orElse(null);
        if(product != null) return product;
        return null;
    }

    @GetMapping("/second-springboot-test")
    public String callSecondSpringboot() {

        String url ="/api-customer/test";
        String accessToken = loginController.getAccessToken();
        if(LoginController.isTokenExpired(accessToken)) loginController.login();

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
        ResponseEntity<String> exchange = secondRestTemplate.exchange(url, HttpMethod.GET, request, String.class);

        return exchange.getBody();
    }
}
