package com.example.demo.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.management.OperationsException;
import java.util.Date;

@RestController
@RequestMapping("/login")
public class LoginController {
    private final RestTemplate restTemplate;
    private final RestTemplate secondRestTemplate;
    @Value("${keycloak.admin-cli.spring-boot-realm.client-secret}")
    private String clientSecret;
    @Value("${keycloak.realm}")
    private String keycloakRealm;
    private String accessToken;

    public String getAccessToken() {
        return this.accessToken;
    }


    @Autowired
    public LoginController( @Qualifier("keycloakRestTemplate") RestTemplate restTemplate,@Qualifier("secondServiceRestTemplate") RestTemplate secondRestTemplate) {
        this.secondRestTemplate = secondRestTemplate;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/test")
    public String test() {
        return "Success";
    }

    @PostMapping("/user")
    public ResponseEntity<String> login() {
        String url = "/realms/" + keycloakRealm + "/protocol/openid-connect/token";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "client_credentials");
        map.add("client_id", "second-spring-client");
        map.add("client_secret", clientSecret);
        map.add("scope", "openid");


        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = null;

        try {
            response = restTemplate.postForEntity(url, request, String.class);
        } catch (HttpClientErrorException e) {
            return new ResponseEntity<>("Invalid user credentials", HttpStatus.UNAUTHORIZED);
        }

        String responseBody = response.getBody();
        JSONObject jsonObj = new JSONObject(responseBody);
        accessToken = jsonObj.getString("access_token");
        return new ResponseEntity<>(jsonObj.toString(), HttpStatus.OK);
    }

    @GetMapping("/second-service-test")
    public String secondServiceTest() {
        String url ="/api-customer/test";

        if(isTokenExpired(accessToken)) login();

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
        ResponseEntity<String> exchange = secondRestTemplate.exchange(url, HttpMethod.GET, request, String.class);

       return exchange.getBody();
    }

    public static boolean isTokenExpired(String accessToken) {
        if(accessToken == null) return true;
        DecodedJWT jwt = JWT.decode(accessToken);
        if( jwt.getExpiresAt().before(new Date())) {
           return true;
        }
        return false;
    }

    @GetMapping("/not-found")
    public void throwNotFoundException() throws ChangeSetPersister.NotFoundException {
        throw new ChangeSetPersister.NotFoundException();
    }

    @GetMapping("/operation-exception")
    public void throwOperationException() throws  OperationsException {
        throw new OperationsException("Mock Operation Exception");
    }

//    @GetMapping("/register-user")
//    public ResponseEntity<String> registerUser(@RequestBody UserRegistration userRegistration) throws JSONException {
//        String adminCliToken = getAdminCliToken();
//
//        String url = "/admin/realms/" + keycloakRealm + "/users";
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.setBearerAuth(adminCliToken);
//        HttpEntity<UserRegistration> request = new HttpEntity<>(userRegistration, headers);
//        ResponseEntity<String> stringResponseEntity = null;
//
//        try {
//            stringResponseEntity = restTemplate.postForEntity(url, request, String.class);
//        } catch (HttpClientErrorException e) {
//            if(e.getMessage().contains("User exists with same username"))
//                return new ResponseEntity<>("User exists with same username", HttpStatus.CONFLICT);
//        }
//
//        return stringResponseEntity;
//    }
//
//    public String getAdminCliToken() throws JSONException {
//        String url = "/realms/" + keycloakRealm + "/protocol/openid-connect/token";
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//        map.add("grant_type", "client_credentials");
//        map.add("client_id", "admin-cli");
//        map.add("client_secret", clientSecret);
//
//        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
//
//        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
//
//        String responseBody = response.getBody();
//        JSONObject jsonObj = new JSONObject(responseBody);
//        return jsonObj.getString("access_token");
//    }
}
