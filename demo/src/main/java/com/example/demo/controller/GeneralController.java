package com.example.demo.controller;

import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/general")
public class GeneralController {

    @GetMapping("/check")
    public boolean checkToken() {
        return true;
    }


}
