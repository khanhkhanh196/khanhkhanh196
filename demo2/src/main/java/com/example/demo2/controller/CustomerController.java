package com.example.demo2.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api-customer")
public class CustomerController {
    @GetMapping("test")
    public String test( ) {
        return "Customer test success";
    }
}
